#!/bin/sh

set -e

########## 宏定义 ####################
# 当前路径宏 ROOT_DIR
# 当前时间宏 DATA_TIME
# log文件路径 LOG_FILE
# 脚本路径 SCRIPT_DIR
# 配置文件路径 ETC_DIR
########## 宏定义 ####################
export ROOT_DIR=$(cd $(dirname $0); pwd)
export DATA_TIME=$(date "+%Y_%m_%d_%H_%M_%S")
export LOG_FILE=${ROOT_DIR}/log_${DATA_TIME}
export SCRIPT_DIR=${ROOT_DIR}/script
export ETC_DIR=${ROOT_DIR}/etc
export LSB_RELEASE=$(echo "$(lsb_release -c)" | awk '{print $2}')

source ${SCRIPT_DIR}/common.sh
source ${SCRIPT_DIR}/vim.sh

function create_log_file() {
    if [ -e ${LOG_FILE} ]; then
        rm -rf ${LOG_FILE}
    fi
    echo -e "create log file ${LOG_FILE}" > ${LOG_FILE}
    touch ${LOG_FILE}
    echo -e "create log file success" >> ${LOG_FILE}
}

function check_sh() {
    echo -e "start check sh..." >> ${LOG_FILE}
    local cnt=$(ls -l /bin/sh | grep dash | wc -l)
    if [[ cnt -gt 0 ]]; then
        echo -e "/bin/sh is link to /bin/dash, will to change to /bin/bash" >> ${LOG_FILE}
        bash -c "rm /bin/sh; ln -s /bin/bash /bin/sh" >> ${LOG_FILE} 2>&1
    else
        echo -e "/bin/sh is link to /bin/bash" >> ${LOG_FILE}
    fi
    echo -e "check sh end..." >> ${LOG_FILE}
}

function check_asm() {
    echo -e "start check asm..." >> ${LOG_FILE}
    pushd /usr/include
    if [ ! -e "asm" ]; then
        ln -s ./x86_64-linux-gnu/asm asm
    fi
    popd
    echo -e "check asm end..." >> ${LOG_FILE}
}

function apt_get_install() {
    echo -e "start apt_get install..." >> ${LOG_FILE}
    local apt_list=${ETC_DIR}/${LSB_RELEASE}/apt_list.md
    if [ ! -f ${apt_list} ]; then
        echo -e "Please set list file dir" >> ${LOG_FILE}
        return 0
    fi

    while read install_cmd; do
        local cnt=$(echo "${install_cmd}" | grep apt | wc -l)
        if [[ ${cnt} -gt 0 ]]; then
            echo -e "start ${install_cmd}" >> ${LOG_FILE}
            bash -c "${install_cmd}" >> ${LOG_FILE} 2>&1
        fi
    done < ${apt_list}
    echo -e "apt_get install end..." >> ${LOG_FILE}
}

function repo_install() {
    echo "start repo install..." >> ${LOG_FILE}
    curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
    chmod a+x repo
    mv repo /usr/local/bin/
    echo "repo install end..." >> ${LOG_FILE}
}

function git_lfs_install() {
    echo -e "start git lfs install..." >> ${LOG_FILE}
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash >> ${LOG_FILE} 2>&1
    apt-get -y -f install git-lfs >> ${LOG_FILE} 2>&1
    git lfs install >> ${LOG_FILE} 2>&1
    echo -e "git lfs install end..." >> ${LOG_FILE}
}

function change_sources_list() {
    echo "start change sources list..." >> ${LOG_FILE}
    local sources_file=${ETC_DIR}/${LSB_RELEASE}/sources.list
    local current_file=/etc/apt/sources.list    
    local list_bak=/etc/apt/sources.list_bak
    local md5file=${ETC_DIR}/md5

    echo "sources_file is ${sources_file}" >> ${LOG_FILE}
    if [ ! -f ${sources_file} ]; then
        echo -e "${sources_file} is not exist" >> ${LOG_FILE}
        exit -1
    fi

    if [ ! -f ${current_file} ]; then
        echo -e "${current_file} is not exist, change new file" >> ${LOG_FILE}
        bash -c "cp ${sources_file} ${current_file}"
        echo "apt-get update start." >> ${LOG_FILE}
        bash -c "apt-get update" >> ${LOG_FILE} 2>&1
        echo "apt-get update success." >> ${LOG_FILE}
        echo "apt-get upgrade start." >> ${LOG_FILE}
        bash -c "apt-get upgrade -y" >> ${LOG_FILE} 2>&1
        echo "apt-get upgrade success." >> ${LOG_FILE}
        echo "change sources list end..." >> ${LOG_FILE}
        return 0
    fi

    bash -c "md5sum ${sources_file} ${current_file} > ${md5file}"
    ret=$(awk 'BEGIN {idx=0} {arr[idx]=$1; idx++} END {if (arr[0] == arr[1]) { print "OK" } else {print "FAILED"}}' ${md5file})
    bash -c "rm -rf ${md5file}"
    if [ ${ret} = "OK" ]; then
        echo "sources list is already change" >> ${LOG_FILE}
        echo "change sources list end..." >> ${LOG_FILE}
        return 0
    fi

    mv ${current_file} ${list_bak} >> ${LOG_FILE} 2>&1
    bash -c "cp ${sources_file} ${current_file}" >> ${LOG_FILE} 2>&1
    echo "apt-get update start." >> ${LOG_FILE}
    bash -c "apt-get update" >> ${LOG_FILE} 2>&1
    echo "apt-get update success." >> ${LOG_FILE}
    echo "apt-get upgrade start." >> ${LOG_FILE}
    bash -c "apt-get upgrade -y" >> ${LOG_FILE} 2>&1
    echo "apt-get upgrade success." >> ${LOG_FILE}
    echo "change sources list end..." >> ${LOG_FILE}
}

function main() {
    # 配置log
    create_log_file

    # 检查当前是否为root账号
    check_root  

    # 更换安装源
    change_sources_list
    
    # 配置sh为bash
    check_sh

    # 配置asm软连接
    check_asm

    # 文件格式全部转换为unix格式
    file_format_check

    # 安装常用工具包
    apt_get_install

    # git lfs安装
    git_lfs_install

    # 配置repo
    repo_install

    # 安装vim
    vim_install
}

main

#!/bin/sh

set -e

export ROOT_DIR=$(cd $(dirname $0); pwd)
export DATA_TIME=$(date "+%Y_%m_%d_%H_%M_%S")
export LOG_FILE=${ROOT_DIR}/user_log_${DATA_TIME}
export SCRIPT_DIR=${ROOT_DIR}/script

source ${SCRIPT_DIR}/vim.sh

function main() {
    # 配置vim
    vim_install
}

main

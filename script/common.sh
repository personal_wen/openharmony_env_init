#!/bin/sh

set -e

function check_root() {
    set +e
    echo -e "start check root..." >> ${LOG_FILE}
    if [[ $(id -u) -ne 0 ]]; then
        echo "current user is not root" >> ${LOG_FILE}
        exit 0
    else
        echo "current user is root" >> ${LOG_FILE}
        set -e
        return 0
    fi
}

function file_format_check() {
    set +e
    echo -e "start check file format..." >> ${LOG_FILE}
    bash -c "dos2unix --version"
    if [[ $? -ne 0 ]]; then
        bash -c "apt-get -f -y install dos2unix" >> ${LOG_FILE} 2>&1
    fi
    set -e
    bash -c "find . -type f | grep -v '/\.' | xargs dos2unix" >> ${LOG_FILE} 2>&1
    echo -e "check file format end..." >> ${LOG_FILE}
}

function set_ps() {
    echo -e "start set ps..." >> ${LOG_FILE}
    echo "PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\n\$ '" >> ~/.bashrc
    echo -e "set ps end..." >> ${LOG_FILE}
}

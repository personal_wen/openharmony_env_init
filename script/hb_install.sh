#!/bin/sh

set -e

# HB_VERSION=0.4.3

function hb_install() {
    bash -c "pip3 uninstall ohos-build -y"
    if [ -z ${HB_VERSION} ]; then
        echo -e "install hb default version!"
        python3 -m pip install --user ohos-build
    else
        echo -e "install hb ${HB_VERSION} version!"
        python3 -m pip install ohos-build==${HB_VERSION}
    fi
}

hb_install

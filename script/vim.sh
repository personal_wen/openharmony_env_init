#!/bin/bash

set -e

function vim_install() {
    echo "start install vim..." >> ${LOG_FILE}
    local i=0
    pushd ~
    set +e
    while [[ $i -le 3 ]]; do
        git clone https://gitee.com/hoeprun-trainning/vim.git >> ${LOG_FILE} 2>&1
        if [[ $? -ne 0 ]]; then
            if [[ $i -eq 0 ]]; then
                apt-get -f -y install gnutls-bin
                git config --global http.sslVerify false
                git config --global http.postBuffer 1048576000
            fi
            let i++
            if [[ $i -eq 3 ]]; then
                echo "git clone vim failed, vim install failed" >> ${LOG_FILE}
                exit -1
            fi
            continue
        fi
        cd vim/vim
        bash build.sh
        cd ~
        rm ~/vim -rf
        break
    done
    set +e

    popd
    echo "install vim end..." >> ${LOG_FILE}
}